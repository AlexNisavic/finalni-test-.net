﻿using AleksandraNisavic.Interfaces;
using AleksandraNisavic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AleksandraNisavic.Controllers
{
    public class PretragaController : ApiController
    {
        INekretnina _repository { get; set; }

        public PretragaController(INekretnina repository)
        {
            _repository = repository;
        }


        public IEnumerable<Nekretnina> PostPretraga(Pretraga pretraga)
        {
            return _repository.Pretraga(pretraga);
        }
    }
}
