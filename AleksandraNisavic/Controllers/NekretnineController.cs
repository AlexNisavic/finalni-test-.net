﻿using AleksandraNisavic.Interfaces;
using AleksandraNisavic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AleksandraNisavic.Controllers
{
    public class NekretnineController : ApiController
    {
        INekretnina _repository { get; set; }

        public NekretnineController(INekretnina repository)
        {
            _repository = repository;
        }

        [Route("api/nekretnine")]
        public IEnumerable<Nekretnina> GetAll()
        {
            return _repository.GetAll();
        }

        public IHttpActionResult GetById(int id)
        {
            var nekretnina = _repository.GetById(id);

            if (nekretnina == null)
            {
                return NotFound();
            }

            return Ok(nekretnina);
        }

        [Route("api/nekretnine")]
        public IHttpActionResult GetByYear(int napravljeno)
        {
            var nekretnine = _repository.GetByYear(napravljeno);

            if (nekretnine == null)
            {
                return NotFound();
            }

            return Ok(nekretnine);
        }

        public IHttpActionResult Post(Nekretnina nekretnina)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Add(nekretnina);

            //return Ok(kosarkas);
            return CreatedAtRoute("DefaultApi", new { id = nekretnina.Id }, nekretnina);
        }

        [Authorize]
        public IHttpActionResult Put(int id, Nekretnina nekretnina)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != nekretnina.Id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(nekretnina);
            }
            catch
            {
                return BadRequest();
            }

            return Ok(nekretnina);
        }

        [Authorize]
        public IHttpActionResult Delete(int id)
        {
            Nekretnina nekretnina = _repository.GetById(id);

            if (nekretnina == null)
            {
                return NotFound();
            }

            _repository.Delete(nekretnina);

            return Ok();
        }
    }
}
