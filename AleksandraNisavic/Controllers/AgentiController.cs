﻿using AleksandraNisavic.Interfaces;
using AleksandraNisavic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AleksandraNisavic.Controllers
{
    public class AgentiController : ApiController
    {
        IAgent _repository { get; set; }

        public AgentiController(IAgent repository)
        {
            _repository = repository;
        }

        public IEnumerable<Agent> GetAll()
        {
            return _repository.GetAll();
        }

        public IHttpActionResult GetById(int id)
        {
            var agent = _repository.GetById(id);

            if (agent == null)
            {
                return NotFound();
            }

            return Ok(agent);
        }

        [Route("api/ekstremi")]
        public IHttpActionResult GetEkstremi()
        {
            List<Agent> agenti = new List<Agent>();

            var a1 = _repository.GetMax();
            var a2 = _repository.GetMin();

            agenti.Add(a1);
            agenti.Add(a2);

            return Ok(agenti);

        }

        [Route("api/najmladji")]
        public IEnumerable<Agent> GetNajmladji()
        {
            return _repository.GetNajmadji();
        }
    }
}
