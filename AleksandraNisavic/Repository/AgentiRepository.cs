﻿using AleksandraNisavic.Interfaces;
using AleksandraNisavic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AleksandraNisavic.Repository
{
    public class AgentiRepository : IAgent, IDisposable
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Agent> GetAll()
        {
            return db.Agents;
        }

        public IEnumerable<Agent> GetNajmadji()
        {
            return db.Agents.OrderByDescending(x => x.GodinaRodjenja);
        }

        public Agent GetById(int id)
        {
            return db.Agents.FirstOrDefault(x => x.Id == id);
        }

        public Agent GetMax()
        {
            return db.Agents.OrderByDescending(x => x.BrojProdatihNekretnina).First();
        }

        public Agent GetMin()
        {
            return db.Agents.OrderBy(x => x.BrojProdatihNekretnina).First();
        }
    }
}