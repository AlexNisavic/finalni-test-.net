﻿using AleksandraNisavic.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using AleksandraNisavic.Interfaces;

namespace AleksandraNisavic.Repository
{
    public class NekretnineRepository : IDisposable, INekretnina
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Nekretnina> GetAll()
        {
            return db.Nekretninas.Include(x => x.Agent).OrderByDescending(p => p.Cena);
        }

        public Nekretnina GetById(int id)
        {
            return db.Nekretninas.Include(a => a.Agent).FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Nekretnina> GetByYear(int year)
        {
            return db.Nekretninas.Include(x => x.Agent).Where(g => g.GodinaIzgradnje > year).OrderBy(o => o.GodinaIzgradnje);
        }

        public void Add(Nekretnina nekretnina)
        {
            db.Nekretninas.Add(nekretnina);
            db.SaveChanges();
        }

        public void Update(Nekretnina nekretnina)
        {
            db.Entry(nekretnina).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void Delete(Nekretnina nekretnina)
        {
            db.Nekretninas.Remove(nekretnina);
            db.SaveChanges();
        }

        public IEnumerable<Nekretnina> Pretraga(Pretraga pretraga)
        {
            return db.Nekretninas.Include(a => a.Agent).Where(n => n.Kvadratura > pretraga.Mini && n.Kvadratura < pretraga.Maksi);
        }
    }
}