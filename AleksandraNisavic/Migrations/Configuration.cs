namespace AleksandraNisavic.Migrations
{
    using AleksandraNisavic.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AleksandraNisavic.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AleksandraNisavic.Models.ApplicationDbContext context)
        {
            context.Agents.AddOrUpdate(a => a.Id,
                    new Agent() { Id = 1, ImeIPrezime = "Pera Peric", Licenca = "Lic1", GodinaRodjenja = 1960, BrojProdatihNekretnina = 15 },
                    new Agent() { Id = 2, ImeIPrezime = "Mika Mikic", Licenca = "Lic2", GodinaRodjenja = 1970, BrojProdatihNekretnina = 10 },
                    new Agent() { Id = 3, ImeIPrezime = "Zika Zikic", Licenca = "Lic3", GodinaRodjenja = 1980, BrojProdatihNekretnina = 5 }
                );

            context.Nekretninas.AddOrUpdate(n => n.Id,
                    new Nekretnina() { Id = 1, Mesto = "Novi Sad", AgencijsakOznaka = "Nek01", GodinaIzgradnje = 1974, Kvadratura = 50m, Cena = 40000m, AgentId = 1 },
                    new Nekretnina() { Id = 2, Mesto = "Beograd", AgencijsakOznaka = "Nek02", GodinaIzgradnje = 1990, Kvadratura = 60m, Cena = 50000m, AgentId = 2 },
                    new Nekretnina() { Id = 3, Mesto = "Subotica", AgencijsakOznaka = "Nek03", GodinaIzgradnje = 1995, Kvadratura = 55m, Cena = 45000m, AgentId = 3 },
                    new Nekretnina() { Id = 4, Mesto = "Zrenjanin", AgencijsakOznaka = "Nek04", GodinaIzgradnje = 2010, Kvadratura = 70m, Cena = 60000m, AgentId = 1 }
                );
        }
    }
}
