﻿using AleksandraNisavic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AleksandraNisavic.Interfaces
{
    public interface INekretnina
    {
        IEnumerable<Nekretnina> GetAll();
        Nekretnina GetById(int id);
        IEnumerable<Nekretnina> GetByYear(int year);
        void Add(Nekretnina nekretnina);
        void Update(Nekretnina nekretnina);
        void Delete(Nekretnina nekretnina);
        IEnumerable<Nekretnina> Pretraga(Pretraga pretraga);
    }
}
