﻿using AleksandraNisavic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AleksandraNisavic.Interfaces
{
    public interface IAgent
    {
        IEnumerable<Agent> GetAll();
        IEnumerable<Agent> GetNajmadji();
        Agent GetById(int id);
        Agent GetMax();
        Agent GetMin();
    }
}
