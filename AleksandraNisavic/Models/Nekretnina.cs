﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AleksandraNisavic.Models
{
    public class Nekretnina
    {
        public int Id { get; set; }
        [Required]
        [StringLength(maximumLength: 40)]
        public string Mesto { get; set; }
        [Required]
        [StringLength(maximumLength: 5)]
        public string AgencijsakOznaka { get; set; }
        [Range(1900, 2018)]
        public int GodinaIzgradnje { get; set; }
        [Required]
        [Range(typeof(decimal), "3", "79228162514264337593543950335")]
        public decimal Kvadratura { get; set; }
        [Required]
        [Range(typeof(decimal), "1", "100000")]
        public decimal Cena { get; set; }

        public Agent Agent { get; set; }
        public int AgentId { get; set; }
    }
}