﻿$(document).ready(function () {

    // podaci od interesa
    var host = window.location.host;
    var token = null;
    var headers = {};
    var agentiUrl = "http://" + host + "/api/agenti/";
    var nekretnineUrl = "http://" + host + "/api/nekretnine/";
    var pretragaUrl = "http://" + host + "/api/pretraga/";
    var formAction = "Create";
    var editingId;

    // pripremanje dogadjaja za brisanje
    $("body").on("click", "#products #btnDelete", deleteNekretnina);

    // pripremanje dogadjaja za izmenu
    $("body").on("click", "#products #btnEdit", editNekretnine);

    // posto inicijalno nismo prijavljeni, sakrivamo odjavu
    $("#odjava").css("display", "none");
    $("#divPretraga").css("display", "none");

    $("#registracijaIPrijava").click(function () {
       
        $("#registracija").css("display", "block");
        $("#registracijaIPrijava").css("display", "none");
    });

    $("#odustajanje").click(function () {
        refreshTable();
        $("#nekretnineForm").css("display", "none");
    });

 

    // registracija korisnika
    $("#registracija").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();
        var loz1 = $("#regLoz").val();
        

        // objekat koji se salje
        var sendData = {
            "Email": email,
            "Password": loz1,
            "ConfirmPassword": loz1
           
        };
        console.log(sendData);

        $.ajax({
            type: "POST",
            url: 'http://' + host + "/api/Account/Register",
            data: sendData

        }).done(function (data) {
            $("#info").append("Uspešna registracija. Možete se prijaviti na sistem.");

        

            }).fail(function (data) {
                alert("Greska Prilikom prijave!");
            alert(data);
        });



    });


    // prijava korisnika
    $("#prijava").click(function () {
      


        var email = $("#regEmail").val();
        var loz1 = $("#regLoz").val();


        // objekat koji se salje
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": loz1
        };

        $.ajax({
            "type": "POST",
            "url": 'http://' + host + "/Token",
            "data": sendData

        }).done(function (data) {
            console.log(data);
            $("#poruka").empty().append("Prijavljen korisnik: " + data.userName);
            token = data.access_token;
            $("#prijava").css("display", "none");
            $("#registracija").css("display", "none");
            $("#odjava").css("display", "block");
            $("#divPretraga").css("display", "block");
            $("#pocetno").css("display", "none");
            $("#btnPrijava").css("display", "none");
            $("#btnRegistracija").css("display", "none");
            // prikaz forme
            jQuery(function () {
                jQuery('#prikazNekretnina').click();

            });


        }).fail(function (data) {
            alert(data);
        });



    });

    // odjava korisnika sa sistema
    $("#odjavise").click(function () {
        token = null;
        headers = {};

        $("#prijava").css("display", "block");
        $("#registracija").css("display", "block");
        $("#odjava").css("display", "none");
        $("#info").empty();
        $("#products #formDiv").css("display", "none");
        location.reload();
    });

    // ucitavanje nekretnina
    $.getJSON(nekretnineUrl, setNekretnine);

    $("#prikazNekretnina").click(function () {
        $.getJSON(nekretnineUrl, setNekretnine);
        $("#products #formDiv").css("display", "block");
    });
    function setNekretnine(data, status) {
        console.log("Status: " + status);

        var $container = $("#products #data");
        $container.empty();

        if (status === "success") {
            console.log(data);
            // ispis naslova
            var div = $("<div></div>");
           
           
            // ispis tabele
            var table = $("<table class='table table-hover table-boarded'></table>");
            if (token) {
                var header = $("<tr style='background-color:yellow;'><th>Mesto </th><th> Oznaka </th><th> Izgradnja </th><th> Kvadratura </th><th> Cena </th><th>Agent</th><th>Brisanje</th><th>Izmena</th></tr>");
            }
            else {
                var header = $("<tr style='background-color:yellow;'><th>Mesto </th><th> Oznaka </th><th> Izgradnja </th><th> Kvadratura </th><th> Cena </th><th>Agent</th><th colspan=\"2\"></th></tr>");
            }
  
            table.append(header);
            console.log(data.length);
            for (i = 0; i < data.length; i++) {
                // prikazujemo novi red u tabeli
                var row = "<tr>";
                // prikaz podataka
                var displayData = "<td>" + data[i].Mesto + "</td><td>" + data[i].AgencijsakOznaka + "</td><td>" + data[i].GodinaIzgradnje + "</td><td>" + data[i].Kvadratura + "</td><td>" + data[i].Cena + "</td><td>" + data[i].Agent.ImeIPrezime +"</td>";
                // prikaz dugmadi za izmenu i brisanje
                var stringId = data[i].Id.toString();
                console.log("Usao sam u for petlju");
                if (token) {
                    var displayDelete = "<td><button id=btnDelete name=" + stringId + " class='btn btn-danger'>Obrisi</button></td>";
                    var displayEdit = "<td><button id=btnEdit name=" + stringId + " class='btn btn-warning'>Izmeni</button></td>";
                    row += displayData + displayDelete + displayEdit + "</tr>";
                }
                else {


                    row += displayData + "</tr>";
                }


                table.append(row);

            }

            div.append(table);
            $container.append(div);
            if (token) {

                $('#agent').empty();
                $('#agent').append('<option value="">-- Select --</option>');


                $.getJSON(agentiUrl, function (data) {
                    $.each(data, function (index, value) {
                        $('#agent').append('<option value ="' + value.Id + '">' + value.ImeIPrezime + '</option>');
                    });
                });
            }
        }


        else {
            div = $("<div></div>");
            h1 = $("<h1>Greška prilikom preuzimanja Nekretnina!</h1>");
            div.append(h1);
            $container.append(div);
        }


    }

    $("#pretraga").click(function () {

        console.log("usao sam u petragu");
        if (token) {
            console.log(token);
            headers.Authorization = 'Bearer ' + token;
        }

        var najmanje = $("#od").val();
        var najvise = $("#do").val();
        var ispunjeno = true;
        var sendData = {
            "Mini": najmanje,
            "Maksi": najvise
        };

        console.log("objekat za slanje");
        console.log(sendData);




        

            $.ajax({
                url: pretragaUrl,
                type: "POST",
                data: sendData,
                "headers": headers
            })
                .done(setNekretnine)
                .fail(function (data, status) {
                    console.log(data);
                    console.log(status);
                    alert("Desila se greska!");
                });
        
    });


    // dodavanje novog proizvoda
    $("#nekretnineForm").submit(function (e) {
        // sprecavanje default akcije forme
        e.preventDefault();

        if (token) {
            console.log(token);
            headers.Authorization = 'Bearer ' + token;
        }

        var agent=$("#agent").val();
        var oznaka = $("#oznaka").val();
        var mesto = $("#mesto").val();
        var godina = $("#godina").val();
        var kvadratura = $("#kvadratura").val();
        var cena = $("#cena").val();

        var httpAction;
        var sendData;
        var url;

     
       
            httpAction = "PUT";
            url = nekretnineUrl + editingId.toString();
            sendData = {
                "Id": editingId,
                "Mesto": mesto,
                "AgencijsakOznaka": oznaka,
                "GodinaIzgradnje": godina,
                "Kvadratura":kvadratura,
                "Cena": cena,
                "AgentId":agent
            };
        


        console.log("Objekat za slanje");
        console.log(sendData);
        console.log(url);
        console.log(httpAction);

        $.ajax({
            url: url,
            type: httpAction,
            data: sendData,
            headers: headers
        })
            .done(function (data, status) {
                formAction = "Create";
                refreshTable();
                $("#nekretnineForm").css("display", "none");
            })
            .fail(function (data, status) {
                console.log(data);
                console.log(status);
                alert("Desila se greska!");
            });

    });

    // brisanje nekretnine
    function deleteNekretnina() {

        if (token) {
            console.log(token);
            headers.Authorization = 'Bearer ' + token;
        }
        // izvlacimo {id}
        var deleteID = this.name;
        // saljemo zahtev 
        //var url = festivalUrl + deleteID.toString();
        //console.log(url);
        $.ajax({
            "url": nekretnineUrl + deleteID.toString(),
            "type": "DELETE",
            "headers": headers
        })
            .done(function (data, status) {
                refreshTable();
            })
            .fail(function (data, status) {
                alert("Desila se greska!");
            });
    }

    // osvezi prikaz tabele
    function refreshTable() {
        $("#agent").val("");
        $("#oznaka").val("");
        $("#mesto").val("");
        $("#godina").val("");
        $("#kvadratura").val("");
        $("#cena").val("");
         //osvezavam
        $.getJSON(nekretnineUrl, setNekretnine);
    }



    function editNekretnine() {

        $("#nekretnineForm").css("display", "block");
        editingId = this.name;
        if (token) {
            console.log(token);
            headers.Authorization = 'Bearer ' + token;
        }

        $.ajax({
            url: nekretnineUrl + editingId.toString(),
            type: "GET",
            "headers": headers
        }).done(function (data, status) {
            $("#agent").val(data.AgentId);
            $("#oznaka").val(data.AgencijsakOznaka);
            $("#mesto").val(data.Mesto);
            $("#godina").val(data.GodinaIzgradnje);
            $("#kvadratura").val(data.Kvadratura);
            $("#cena").val(data.Cena);

            editingId = data.Id;
            formAction = "Update";
        }).fail(function (data, status) {
            formAction = "Create";
            alert("Error");
        });
    }

});