﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AleksandraNisavic.Interfaces;
using AleksandraNisavic.Models;
using AleksandraNisavic.Controllers;
using System.Web.Http;
using Moq;
using System.Web.Http.Results;
using System.Linq;

namespace AleksandraNisavic.Tests.Controllers
{
    [TestClass]
    public class NekretnineControllerTtest
    {
        [TestMethod]
        public void GetReturnsNekretninaWithSameId()
        {
            // Arrange
            var mockRepository = new Mock<INekretnina>();

            mockRepository.Setup(x => x.GetById(42)).Returns(new Nekretnina { Id = 42 });

            var controller = new NekretnineController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetById(42);
            var contentResult = actionResult as OkNegotiatedContentResult<Nekretnina>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.Id);
        }

        [TestMethod]
        public void DeleteReturnsNotFound()
        {
            // Arrange 
            var MockRepository = new Mock<INekretnina>();
            var controller = new NekretnineController(MockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Delete(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));

        }

        [TestMethod]
        public void PutReturnsBadRequest()
        {
            // Arrange
            var mockRepository = new Mock<INekretnina>();
            var controller = new NekretnineController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Put(10, new Nekretnina { Id = 9, Mesto = "Test", Cena = 100m, AgentId = 1, AgencijsakOznaka="Nek01", GodinaIzgradnje=1999,  Kvadratura= 50m});

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }

        [TestMethod]
        public void GetReturnsMultipleObjects()
        {
            List<Nekretnina> nekretnine = new List<Nekretnina>();
            Pretraga p1 = new Pretraga() { Mini = 0, Maksi = 110 };
            nekretnine.Add(new Nekretnina { Id = 9, Mesto = "Test", Cena = 100m, AgentId = 1, AgencijsakOznaka = "Nek01", GodinaIzgradnje = 1999,  Kvadratura = 50m});
            nekretnine.Add(new Nekretnina { Id = 10, Mesto = "Test1", Cena = 100m, AgentId = 1, AgencijsakOznaka = "Nek01", GodinaIzgradnje = 1999, Kvadratura = 50m });
            var mockRepository = new Mock<INekretnina>();
            mockRepository.Setup(x => x.GetAll()).Returns(nekretnine.AsEnumerable());
            var controller = new PretragaController(mockRepository.Object);

            //act

            IEnumerable<Nekretnina> result = controller.PostPretraga(p1);

            //Assert

            Assert.IsNotNull(result);
            Assert.AreEqual(nekretnine.Count, result.ToList().Count);
            Assert.AreEqual(nekretnine.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(nekretnine.ElementAt(1), result.ElementAt(1));
        }
    }
}
